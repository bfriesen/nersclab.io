# Current known issues

## Occasional hanging on Cori login nodes

The cscratch1 crash at the end of September left a number of broken 
inodes (file and directory metadata) on the filesystem, which we are 
still in the process of cleaning up. Interactions with these files 
can get stuck, eventually causing the login node to appear to hang,
either while logging in or accessing cscratch1. We are monitoring 
for this and rebooting nodes that get stuck - if you encounter the 
problem, wait a few minutes then try again.

Users on impacted nodes will be notified via `wall` to give some
advance notice of a reboot when possible. The reboot will logout users
and kill any current process (including `xfer` jobs running on that
node).

## Python 3.8 Users on GPFS File Systems may observe BlockingIOErrors

Starting in October 2020, users of Python and Jupyter at NERSC began observing `BlockingIOError` failures.
The issue has been identified and is being addressed by the vendor.
Please read more about the issue at the [Python FAQ.](development/languages/python/faq-troubleshooting.md)
