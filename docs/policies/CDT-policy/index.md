# Cray Programming Environment (CDT) Software Update Policy

## Introduction

In order to provide a predictable, stable and consistent programming environment while still making necessary software updates, NERSC updates the Cray Programming Environment, namely the Cray Developer Toolkit (CDT), and the Intel compilers on a set cadence on Cori. CDT consists of compilers, MPI, scientific and I/O libraries, profiling and debugging tools, etc. See [monthly CDT release notes](https://pubs.cray.com/browse/xc/article/released-cray-xc-programming-environments) for full list of software in each CDT.

## Policy 

* The default CDT and Intel compiler versions on Cori will normally be updated once per year in January at the beginning of the new NERSC allocation year.
      * We may elect to continue with the current default, especially if we made a mid-year change in defaults (e.g., due to a major operating system upgrade, or a critical security flaw).    
      * When possible, we will keep the previous default available for the following year.   

* While we strive for continuity of programming environments across an entire allocation year, there may be situations in which we must change the software environment during the year. These situations include major operating system upgrades, critical security flaws, and critical software bugs.

* We can keep a limited number of CDT and Intel modules available as non-default programming environments. We aim to update the available non-default environments every 3 months, eg to provide fixes for specific issues.

* Seven days advance notice will be given prior to making the changes described above, except as needed for critical security or other concerns.

