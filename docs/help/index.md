# NERSC Help and Support

NERSC strives to be the most user friendly supercomputing center in
the world.

## FAQ

* [Password Resets](../accounts/passwords.md#forgotten-passwords)
* [Connection problems](../connect/ssh.md)
* [File Permissions](../filesystems/unix-file-permissions.md)

## Help Desk

The [online help desk](https://help.nersc.gov/) is the **preferred**
method for contacting NERSC.

!!! attention
	NERSC Consultants handle thousands of support requests per
	year. In order to ensure efficient timely resolution of issues
	include **as much of the following as possible** when making a
	request:

	* error messages
	* jobids
	* location of relevant files
	     * input/output
	     * job scripts
	     * source code
	     * executables
	* output of `module list`
	* any steps you have tried
	* steps to reproduce

!!! tip
    You can make code snippets, shell outputs, etc in your ticket much more 
    readable by inserting a line with:
    ```
    [code]<pre>
    ```
    before the snippet, and another line with:
    ```
    </pre>[/code]
    ```
    after it. (For a full list of formatting options, see 
    [this ServiceNow article](https://community.servicenow.com/community?id=community_blog&sys_id=4d9ceae1dbd0dbc01dcaf3231f9619e1))

Access to the online help system requires logging in with your NERSC username,
password, and one-time password. If you are an existing user unable to log in,
you can send an email to <accounts@nersc.gov> for support.

If you are not a NERSC user, you can reach NERSC with your queries at
<accounts@nersc.gov> or <allocations@nersc.gov>.

### Phone support

**Consulting and account-support phone services have been suspended.**

To report an urgent system issue, you may call NERSC at 1-800-66-NERSC
(USA) or 510-486-8600 (local and international).
