# Spin Getting Started Guide (Rancher 2)


## SpinUp Workshops

NERSC conducts an instructor-led SpinUp Workshop for New Users
several times per year that include an overview of Spin concepts and
terms, a series of interactive exercises, and guidance on the design
and development process.

For current users of Spin, or users joining NERSC projects already
using Spin, there is also a Self-Guided SpinUp alternative. This
slightly abbreviated program covers concepts and terms and includes
the same interactive exercises as the instructor-led workshop.
_NERSC approval is required for this option._

## Download SpinUp Workshop Materials 

The materials for the SpinUp Workshop for New Users and Self-Guided
SpinUp are simple slide decks with embedded videos. Either version
provides a simple but thorough guide to get started.

Download the workshop materials at the
[Spin Training and Tutorials](https://www.nersc.gov/users/training/spin/)
page.
